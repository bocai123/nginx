# 根据state的值设置图片地址
if [[ "$DRONE_BUILD_STATUS" == success ]]; then
    image_url="https://zfile.xn--3prx04a3rm7grta.top/directlink/other/success.jpg"
else
    image_url="https://zfile.xn--3prx04a3rm7grta.top/directlink/other/failure.jpg"
fi

timestamp=$(date +%s)  # 获取当前时间戳
timestamp_plus_8=$((timestamp+28800))  # 加上8小时（8 * 3600秒 = 28800秒）
formatted_time=$(date -d @$timestamp_plus_8 +'%-H点%-M分%-S秒')  # 将时间戳转换为指定格式的时间

# 构建JSON数据
json='{
 "msgtype": "markdown",
 "markdown": {
     "title":"'$message'",
     "text": "#### '$message' \n > ###### 代码仓库['$DRONE_REPO']('$DRONE_REPO_LINK') \n > ###### 提交者:'$GIT_COMMITTER_NAME' \n > ###### commit信息:'$CI_COMMIT_MESSAGE'\n > ![screenshot]('$image_url')\n > ###### '$formatted_time'发布 "
 }
}'

# 使用curl发送POST请求，将JSON数据发送到钉钉机器人
curl ''$webhook_url'' \
 -H 'Content-Type: application/json' \
 -d "$json"

